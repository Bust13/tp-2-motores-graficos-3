using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Muerte : MonoBehaviour
{
    public Rigidbody jugador;
    public Text textoGameOver;
    private void Start()
    {
        textoGameOver.gameObject.SetActive(false);
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemigo"))
        {
            Destroy(jugador);
            textoGameOver.gameObject.SetActive(true);
            textoGameOver.text = "Game Over";
            Time.timeScale = 0;

        }

    }
}
