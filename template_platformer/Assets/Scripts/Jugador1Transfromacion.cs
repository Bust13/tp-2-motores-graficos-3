using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Jugador1Transfromacion : MonoBehaviour
{
     public bool transformarse;
    public GameObject rb;
    public GameObject jugador;
    public Text textoGameOver;
    private void Start()
    {
        textoGameOver.gameObject.SetActive(false);
    }
    void Update()
    {
        Transformarse();
        Volver();
    }
    void Transformarse()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            gameObject.transform.localScale = new Vector3(3, 3, 1);
            transformarse = true;
        }
    }
    void Volver()
    {
        if (Input.GetKeyUp(KeyCode.F))
        {
            gameObject.transform.localScale = new Vector3(1, 1, 1);
            transformarse = false;
        }

    }
    public void OnCollisionEnter(Collision collision)
    {
        if (transformarse)
        {
            if (collision.gameObject.CompareTag("Enemigo"))
            {

                Destroy(rb);
            }
        }
        else
        {
            if (collision.gameObject.CompareTag("Enemigo"))
            {
                if (collision.gameObject.CompareTag("Enemigo"))
                {
                    Destroy(this.jugador);
                    textoGameOver.gameObject.SetActive(true);
                    textoGameOver.text = "Game Over";
                    Time.timeScale = 0;
                    
                }
                
            }
        }

    }
}

