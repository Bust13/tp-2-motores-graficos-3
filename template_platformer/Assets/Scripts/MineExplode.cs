using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MineExplode : MonoBehaviour
{
    private GameObject Mine;
    
    public Text textoGameOver;
    void Start()
    {
        GetComponent<ParticleSystem>().Stop();

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GetComponent<ParticleSystem>().Play();
         
            textoGameOver.gameObject.SetActive(true);
            textoGameOver.text = "Game Over";
            Time.timeScale = 0;
        }
    }
    
}
