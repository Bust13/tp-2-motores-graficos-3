using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JugadorPasaPorPunto : MonoBehaviour
{
   
    public Animator animator;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            animator.Play("EnemigoCaeDCielo");
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            animator.Play("EnemigoVuelve");
        }
    }

}
