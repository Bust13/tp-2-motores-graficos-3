using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Estirar : MonoBehaviour
{
    public Animator estirar;
    public bool seEstiro=false;
    public int contadorG=0;

    private void Start()
    {
       estirar= GetComponent<Animator>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            contadorG++;
            if (seEstiro && contadorG >= 2)
            {
                VolverANormalidad();
                seEstiro = false;
                contadorG= 0;   
            }

            else
            {
                Estira();
                seEstiro = true;
            }
        
         

        }
    }

   

    private void Estira()
    {
        estirar.Play("Estirar");
        
    }
    private void VolverANormalidad()
    {
        estirar.Play("Volver");
    }

}
