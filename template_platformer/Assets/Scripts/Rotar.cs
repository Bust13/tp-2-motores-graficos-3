using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotar : MonoBehaviour
{
    public Animator rotar;
    public bool roto = false;
    public int contadorC = 0;

    private void Start()
    {
        rotar = GetComponent<Animator>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            contadorC++;
            if (roto && contadorC >= 2)
            {
                VolverANormalidad();
                roto = false;
                contadorC = 0;
            }

            else
            {
                Rota();
                roto = true;
            }



        }
    }



    private void Rota()
    {
        rotar.Play("RotaAbajo");

    }
    private void VolverANormalidad()
    {
        rotar.Play("RotaArriba");
    }

}
